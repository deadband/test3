﻿using System.Collections.Generic;
using System.Linq;

namespace ResultsDb
{
    public class PlayerRepo
    {
        private GameDbContext _dbContext;

        public PlayerRepo(GameDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddPlayer(Player player)
        {
            _dbContext.Players.Add(player);
            _dbContext.SaveChanges();
        }

        public IList<Player> GetAllPlayers()
        {
            return _dbContext.Players.ToList();
        }
    }
}