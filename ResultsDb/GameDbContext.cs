﻿using System.Data.Entity;

namespace ResultsDb
{
    public class GameDbContext : DbContext
    {
        public DbSet<Player> Players { get; set; }

        public GameDbContext() : base("GameDbConnectionString")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<GameDbContext, Migrations.Configuration>());
        }
    }
}