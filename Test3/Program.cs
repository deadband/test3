﻿using System;
using System.Threading;
using GameLogic;

namespace Test3
{
    class Program
    {
        static void Main(string[] args)
        {
            var guessGame = new GuessGame();

            var name = string.Empty;
            while (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
            {
                Console.Write("Podaj imie gracza: " );
                name = Console.ReadLine();
            }

            var guessResult = GuessResult.mniej;
            while (guessResult != GuessResult.zgadłes)
            {
                var parsePossible = false;
                var guess = -1;
                while (!parsePossible || guess > guessGame.Max || guess < guessGame.Min)
                {
                    Console.Clear();
                    Console.WriteLine("Zgadnij losową liczbę z przedziału <0-1000>");
                    var guessString = Console.ReadLine();
                    parsePossible = int.TryParse(guessString, out guess);
                }
                guessResult = guessGame.Guess(guess);
                if (guessResult == GuessResult.zgadłes)
                {
                    Console.WriteLine($"Gratualcje {name} zgadles po {guessGame.NumberOfTries} próbach!");
                }
                else
                {
                    Console.WriteLine($"To byla twoja {guessGame.NumberOfTries} próba. Musisz dać {guessResult}");
                }
                Thread.Sleep(1500);
            }
            guessGame.AddScore(name, guessGame.NumberOfTries);           
            Console.Clear();
            Console.WriteLine("NAJLEPSZE WYNIKI");
            Console.WriteLine(guessGame.DisplayHighscores());
            Console.ReadKey();
        }
    }
}