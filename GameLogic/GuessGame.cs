﻿using ResultsDb;
using System;
using System.Linq;

namespace GameLogic
{
    public class GuessGame
    {
        public int Max { get; }
        public int Min { get; }
        private readonly GameDbContext _dbContext;
        private readonly PlayerRepo _playerRepo;
        private Random _random;
        private readonly int _numberToGuess;
        private int _numberOfTries;
        public int NumberOfTries { get { return _numberOfTries; } }
        
        public GuessGame()
        {
            _dbContext = new GameDbContext();
            _playerRepo = new PlayerRepo(_dbContext);
            Min = 0;
            Max = 1000;
            _random = new Random();
            _numberToGuess = _random.Next(Min, Max +1);
            _numberOfTries = 0;
        }

        public GuessResult Guess(int num)
        {
            _numberOfTries++;
            if (num > _numberToGuess)
            {
                return GuessResult.mniej;
            }
            if (num < _numberToGuess)
            {
                return GuessResult.więcej;
            }
            else return GuessResult.zgadłes;

        }

        public void AddScore(string name, int score)
        {
            var player = new Player()
            {
                Name = name,
                Score = score
            };
            _playerRepo.AddPlayer(player);
        }

        public string DisplayHighscores()
        {
            var highScores = _playerRepo.GetAllPlayers().OrderBy(p => p.Score).Take(10).ToList();
            var list = "  GRACZ - LICZBA PROB\n";
            for (int i = 0; i < highScores.Count(); i++)
            {
                list = list + $"{i + 1}.  {highScores[i].Name} - {highScores[i].Score}\n";
            }
            return list;
        }
    }
}